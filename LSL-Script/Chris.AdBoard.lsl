integer m_expiryDate        =   0;
key     m_currentRenter     =   NULL_KEY;
list    m_inventarWhitelist =   [".Chris.AdBoard.Free", ".Chris.AdBoard.Infos", ".Chris.AdBoard.Config"];

integer m_channel_socket    =   0;
integer m_channel_number    =   0;

integer m_timer_mode_listen =   FALSE;
integer m_timer_mode_insert =   FALSE;
integer m_timer_mode_text   =   FALSE;

list    m_config            =   []; 

list readConfigFromNC(string NCName)
    {
        list    configFileObject    =    [];
        
        string  configNCContend     =    osGetNotecard(NCName);
        
        configNCContend            =    osReplaceString(configNCContend, "\r", "", -1, 0);
        configNCContend            =    osReplaceString(configNCContend, "\n", ";", -1, 0);
        configNCContend            =    osReplaceString(configNCContend, "$", ";", -1, 0);
        
        list    configLine        =    llParseString2List(configNCContend, [";"], []);
        integer configLineCount    =    llGetListLength(configLine);
        
        addToList(configFileObject, "$ConfigNCName}|{" + NCName);
        
        while(configLineCount--)
        {
            list    lineData    =    llParseString2List(llList2String(configLine, configLineCount), [" = "], []);
            configFileObject    =    addToList(configFileObject, llToLower(llList2String(lineData, 0)) + "}|{" + llList2String(lineData, 1));
        }
        
        return configFileObject;
    }

string getConfig(list configObject, string lineName)
    {
        integer configEntrys    =    llGetListLength(configObject);
        
        while(configEntrys--)
        {
            list    data    =    llParseString2List(llList2String(configObject, configEntrys), ["}|{"], []);
            
            if(llList2String(data, 0) == lineName)
            {
                return llList2String(data, 1);
            }
        }
        
        return "";
    }

integer isInList(list lslList, string entry)
    {
        integer     avatar_count    =    llGetListLength(lslList);
        
        while(avatar_count--)
        {
            if(llList2String(lslList, avatar_count) == entry)
            {
                return TRUE;
            }
        }
        
        return FALSE;   
    }
    
list addToList(list lslList, string entry)
    {
        list newList    =    lslList;
        newList            +=    [(string)entry];
        
        return newList;
    }

setFree()
{
    m_currentRenter  =   NULL_KEY;
    m_expiryDate     =   0;
    clearInventory();
    llSetTexture(getConfig(m_config, "freetexturename"), ALL_SIDES); 
    showText();
}

showText()
{
    if(m_currentRenter != NULL_KEY)
    {
        integer _days = m_expiryDate / 60 / 60 / 24;
        llSetText("Time left: " + (string)_days, <1, 1, 1>, 1.0);
        m_timer_mode_text = TRUE;
    }else{
        llSetText("", <1, 1, 1>, 1.0);
    }
}

clearInventory()
{
    integer _inventoryItems = llGetInventoryNumber(INVENTORY_ALL);
    
    while(_inventoryItems--)
    {
        string _currentInventarName = llGetInventoryName(INVENTORY_ALL, _inventoryItems);
        
        if(!isInList(m_inventarWhitelist, _currentInventarName))
        {
            if(llGetScriptName() != _currentInventarName)
            {
                llRemoveInventory(_currentInventarName);
            }
        }
    }
}

sendAllInventoryItems(key _avatar)
{
    integer _inventoryItems =   llGetInventoryNumber(INVENTORY_ALL);
    list    _sendItems      =   [];
    
    while(_inventoryItems--)
    {
        string _currentInventarName = llGetInventoryName(INVENTORY_ALL, _inventoryItems);
        
        if(!isInList(m_inventarWhitelist, _currentInventarName))
        {
            if(llGetScriptName() != _currentInventarName)
            {
                _sendItems += _currentInventarName;
            }
        }
    }
    
    llGiveInventoryList(_avatar, "AdBoard", _sendItems);
}

string getFirstImage()
{
    integer _inventoryItems = llGetInventoryNumber(INVENTORY_TEXTURE);
    
    while(_inventoryItems--)
    {
        string _currentInventarName = llGetInventoryName(INVENTORY_TEXTURE, _inventoryItems);
        
        if(!isInList(m_inventarWhitelist, _currentInventarName))
        {
            if(llGetScriptName() != _currentInventarName)
            {
                return _currentInventarName;
            }
        }
    }
    
    return "";
}

integer random_integer(integer min, integer max)
{
    return min + (integer)(llFrand(max - min + 1));
}

default
{
    state_entry()
    {
        m_config = readConfigFromNC(".Chris.AdBoard.Config");
        llSetPayPrice(PAY_HIDE, [(integer)getConfig(m_config, "payamount") ,PAY_HIDE, PAY_HIDE, PAY_HIDE]);
        setFree();
        llSetTimerEvent(60);
    }
    
    listen(integer channel, string name, key id, string message )
    {
        if(message == "Infos")   
        {
            llGiveInventory(id, getConfig(m_config, "infonotecardname"));   
        }
           
        if(message == "Unrent")   
        {
            if(llGetOwner() == id || m_currentRenter == id)
            {
                setFree(); 
            }
        }
        
        if(message == "Rent")   
        {
            if(llGetOwner() == id || m_currentRenter == NULL_KEY)
            {
                if(getConfig(m_config, "payenable") == "true")
                {
                    llSay(0, "Pleasy pay this board to rent it.");
                }else{
                    m_currentRenter = id;
                    m_expiryDate = (integer)getConfig(m_config, "expandtime") * 86400;
                }
            }
        }
           
        if(message == "Extend")   
        {
            if(llGetOwner() == id || m_currentRenter == id)
            {
                if(getConfig(m_config, "payenable") == "true")
                {
                    llSay(0, "Pleasy pay this board to extend it.");
                }else{
                    m_expiryDate = m_expiryDate + ((integer)getConfig(m_config, "expandtime") * 86400);  
                }
            }  
        }
        
        if(message == "Insert")   
        {
            if(llGetOwner() == id || m_currentRenter == id)
            {
                m_timer_mode_insert = TRUE;
                llAllowInventoryDrop(TRUE);
                llSetTimerEvent(60);
            }
        }
    }
    
    
    touch_start(integer i)
    {
        m_channel_number    =   random_integer(11111, 99999);
        m_channel_socket    =   llListen(m_channel_number,"", "","");;
        m_timer_mode_listen =   TRUE;
        llSetTimerEvent(60);
        
        if(llGetOwner() == llDetectedKey(0))
        {
            llDialog(llDetectedKey(0), "Select an option what you want do.", ["Rent", "Unrent", "Insert", "Extend", "Exit"], m_channel_number);
            showText();
        }else{
            if(llDetectedKey(0) != m_currentRenter)
            {
                if(m_currentRenter == NULL_KEY)
                {
                    llDialog(llDetectedKey(0), "Select an option what you want do.", ["Infos", "Rent", "Exit"], m_channel_number);
                }else{
                    sendAllInventoryItems(llDetectedKey(0));
                }
            }else{
                 llDialog(llDetectedKey(0), "Select an option what you want do.", ["Insert", "Unrent", "Extend", "Exit"], m_channel_number);
                 showText();
            }
        }
    }
    
    changed(integer change)
    {
        if (change & CHANGED_INVENTORY)         
        {
            m_config = readConfigFromNC(".Chris.AdBoard.Config");
            llSetPayPrice(PAY_HIDE, [(integer)getConfig(m_config, "payamount") ,PAY_HIDE, PAY_HIDE, PAY_HIDE]);
            llSetTexture(getFirstImage(), ALL_SIDES); 
        }
    }
    
    money(key id, integer amount) 
    {
        if(m_currentRenter == NULL_KEY && amount == (integer)getConfig(m_config, "payamount"))
        {
            llSay(0, "Thank you for renting.");   
            m_currentRenter = id;
            m_expiryDate = (integer)getConfig(m_config, "expandtime") * 86400;
        }else{
            if(amount == (integer)getConfig(m_config, "payamount"))
            {
                m_expiryDate = m_expiryDate + ((integer)getConfig(m_config, "expandtime") * 86400);
            }
        }
    }
    
    timer()
    {
        if(m_timer_mode_listen)
        {
            m_timer_mode_listen = FALSE;
            llListenRemove(m_channel_socket);
        }
        
        if(m_timer_mode_insert)
        {
            m_timer_mode_insert = FALSE;
            llAllowInventoryDrop(FALSE);
        }
        
        if(m_timer_mode_text)
        {
            m_timer_mode_text = FALSE;
            llSetText("", <0, 0, 0>, 1.0);
        }
        
        if(m_expiryDate <= 0)
        {
            setFree(); 
        }else{
            m_expiryDate = m_expiryDate - 60;
        }
    }
}